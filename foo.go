package main

import (
	"golang.org/x/crypto/ssh"
)


func bar() {
	_ = ssh.InsecureIgnoreHostKey()
}

func foo() {
	_ = ssh.InsecureIgnoreHostKey()
}
